@food-supplier
Feature: Displaying food suppliers
  In order to be manage food suppliers
  As an administrator
  I have to be able to see them

  Scenario: Displaying food suppliers
    Given the database is empty
    And there is food supplier with parameters:
      | name       | deliveryCost | freeDeliveryThreshold | singlePackageCost | phoneNumber | websiteUrl          | menuUrl                 |
      | supplier-1 | 10.00        | 50.00                 | 5.00              | 123-456-789 | http://supplier.com | http://some-menu-url.pl |
    When I am on "/food-suppliers"
    Then there should be supplier with parameters on the list:
      | name       | deliveryCost | freeDeliveryThreshold | singlePackageCost | phoneNumber | websiteUrl          | menuUrl                 |
      | supplier-1 | 10.00        | 50.00                 | 5.00              | 123-456-789 | http://supplier.com | http://some-menu-url.pl |

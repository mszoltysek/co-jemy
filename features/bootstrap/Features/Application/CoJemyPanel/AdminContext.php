<?php

namespace Features\Application\CoJemyPanel;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Behat\Symfony2Extension\Context\KernelDictionary;
use Bundle\CoJemyCore\CoreBundle\Entity\FoodSupplier;
use Features\Application\Page\FoodSupplier\AddFoodSupplierPage;
use Features\Application\Page\FoodSupplier\EditFoodSupplierPage;
use Features\Application\Page\FoodSupplier\FoodSupplierPage;
use Features\Application\Page\FoodSupplier\FoodSuppliersListPage;
use Features\Application\Page\MenuItem\AddMenuItemPage;
use Features\Application\Page\MenuItem\EditMenuItemPage;
use Tests\Builder\FoodSupplierBuilder;

/**
 * Defines application features from the specific context.
 */
class AdminContext implements Context
{
    use KernelDictionary;
    
    const DEFAULT_FOOD_SUPPLIER_ID = 1;
    const DEFAULT_MENU_ITEM_ID = 1;
    
    /**
     * @var AddFoodSupplierPage
     */
    private $addFoodSupplierPage;

    /**
     * @var EditFoodSupplierPage
     */
    private $editFoodSupplierPage;

    /**
     * @var FoodSuppliersListPage
     */
    private $foodSuppliersListPage;

    /**
     * @var FoodSupplierPage
     */
    private $foodSupplierPage;

    /**
     * @var AddMenuItemPage
     */
    private $addMenuItemPage;

    /**
     * @var EditMenuItemPage
     */
    private $editMenuItemPage;

    /**
     * @param AddFoodSupplierPage $addFoodSupplierPage
     * @param FoodSuppliersListPage $foodSuppliersListPage
     * @param FoodSupplierPage $foodSupplierPage
     * @param EditFoodSupplierPage $editFoodSupplierPage
     * @param AddMenuItemPage $addMenuItemPage
     * @param EditMenuItemPage $editMenuItemPage
     */
    public function __construct(
        AddFoodSupplierPage $addFoodSupplierPage,
        FoodSuppliersListPage $foodSuppliersListPage,
        FoodSupplierPage $foodSupplierPage,
        EditFoodSupplierPage $editFoodSupplierPage,
        AddMenuItemPage $addMenuItemPage,
        EditMenuItemPage $editMenuItemPage
    ) {
        $this->addFoodSupplierPage = $addFoodSupplierPage;
        $this->foodSuppliersListPage = $foodSuppliersListPage;
        $this->foodSupplierPage = $foodSupplierPage;
        $this->editFoodSupplierPage = $editFoodSupplierPage;
        $this->addMenuItemPage = $addMenuItemPage;
        $this->editMenuItemPage = $editMenuItemPage;
    }

    /**
     * @Given there is food supplier with parameters:
     */
    public function thereIsFoodSupplierWithParameters(TableNode $table)
    {
        $this->createFoodSupplier($table->getHash()[0]);
    }

    /**
     * @Given there are food suppliers with parameters:
     */
    public function thereAreFoodSuppliersWithParameters(TableNode $table)
    {
        foreach ($table->getHash() as $supplierHash) {
            $this->createFoodSupplier($supplierHash);
        }
    }

    /**
     * @Given there is food supplier
     */
    public function thereIsFoodSupplier()
    {
        $this->createFoodSupplier();
    }

    /**
     * @Given there is food supplier with menu item with parameters:
     */
    public function thereIsFoodSupplierWithMenuItemWithParameters(TableNode $table)
    {
        $this->createFoodSupplierWithMenuItems($table->getHash());

        $this->foodSupplierPage->open(['id' => self::DEFAULT_FOOD_SUPPLIER_ID]);

        if (!$this->foodSupplierPage->hasMenuItems()) {
            throw new \RuntimeException('There are no menu items.');
        }
    }

    /**
     * @Given /^food supplier with id "([^"]*)" has menu items with parameters:$/
     */
    public function foodSupplierWithIdHasMenuItemsWithParameters($id, TableNode $table)
    {
        $this->createMenuItemsForFoodSupplierWithId($id, $table);
    }

    /**
     * @Given there is food supplier with menu item
     */
    public function thereIsFoodSupplierWithMenuItem()
    {
        $this->createFoodSupplierWithMenuItems();
        
        $this->foodSupplierPage->open(['id' => self::DEFAULT_FOOD_SUPPLIER_ID]);

        if (!$this->foodSupplierPage->hasMenuItems()) {
            throw new \RuntimeException('There are no menu items.');
        }
    }

    /**
     * @When I fill create menu item form with parameters:
     */
    public function iFillCreateMenuItemFormWithParameters(TableNode $table)
    {
        if (!$this->addMenuItemPage->isOpen(['id' => self::DEFAULT_FOOD_SUPPLIER_ID]) ) {
            throw new \RuntimeException('This is not menu item add page.');
        }

        $parameters = $table->getHash();

        $this->addMenuItemPage->fillMenuItemForm($parameters[0]);
    }

    /**
     * @When I fill update menu item form with parameters:
     */
    public function iFillUpdateMenuItemFormWithParameters(TableNode $table)
    {
        if (!$this->editMenuItemPage->isOpen([
            'foodSupplierId' => self::DEFAULT_FOOD_SUPPLIER_ID,
            'menuItemId' => self::DEFAULT_MENU_ITEM_ID
        ])) {
            throw new \RuntimeException('This is not menu item edit page.');
        }

        $parameters = $table->getHash();

        $this->editMenuItemPage->fillMenuItemForm($parameters[0]);
    }

    /**
     * @When I fill supplier add form with parameters:
     */
    public function iFillSupplierAddFormWithParameters(TableNode $table)
    {
        if (!$this->addFoodSupplierPage->isOpen() ) {
            throw new \RuntimeException('This is not food supplier add page.');
        }

        $parameters = $table->getHash();

        $this->addFoodSupplierPage->fillFoodSupplierForm($parameters[0]);
    }

    /**
     * @When I fill supplier edit form with parameters:
     */
    public function iFillSupplierEditFormWithParameters(TableNode $table)
    {
        if (!$this->editFoodSupplierPage->isOpen(['id' => self::DEFAULT_FOOD_SUPPLIER_ID]) ) {
            throw new \RuntimeException('This is not food supplier edit page.');
        }

        $parameters = $table->getHash();

        $this->editFoodSupplierPage->fillFoodSupplierForm($parameters[0]);
    }

    /**
     * @Then I should be on food suppliers list page
     */
    public function iShouldBeOnFoodSuppliersListPage()
    {
        if (!$this->foodSuppliersListPage->isOpen() ) {
            throw new \RuntimeException('This is not food suppliers list page.');
        }
    }

    /**
     * @Then I should be on food supplier page
     */
    public function iShouldBeOnFoodSupplierPage()
    {
        if (!$this->foodSupplierPage->isOpen(['id' => self::DEFAULT_FOOD_SUPPLIER_ID]) ) {
            throw new \RuntimeException('This is not food supplier page.');
        }
    }

    /**
     * @Then there should be no food suppliers
     */
    public function thereShouldBeNoFoodSuppliers()
    {
        if ($this->foodSuppliersListPage->hasFoodSuppliers() ) {
            throw new \RuntimeException('There are some food suppliers.');
        }
    }
    
    /**
     * @Then there should be no menu items
     */
    public function thereShouldBeNoMenuItems()
    {
        $this->foodSupplierPage->isOpen(['id' => self::DEFAULT_FOOD_SUPPLIER_ID]);

        if ($this->foodSupplierPage->hasMenuItems()) {
            throw new \RuntimeException('There are menu items.');
        }
    }

    /**
     * @Then there should be supplier with parameters:
     */
    public function thereShouldBeSupplierWithParameters(TableNode $table)
    {
        $parameters = $table->getHash();
        $this->foodSupplierPage->checkForSupplierWithParameters($parameters[0]);
    }
    
    /**
     * @Then there should be supplier with parameters on the list:
     */
    public function thereShouldBeSupplierWithParametersOnTheList(TableNode $table)
    {
        $parameters = $table->getHash();

        if (!$this->foodSuppliersListPage->isOpen() ) {
            throw new \RuntimeException('This is not food supplier list page.');
        }
        
        $this->foodSuppliersListPage->checkForSupplierWithParameters($parameters[0]);
    }

    /**
     * @Then there should be menu item with parameters on food supplier page:
     */
    public function thereShouldBeMenuItemWithParametersOnFoodSupplierPage(TableNode $table)
    {
        $parameters = $table->getHash();

        $this->foodSupplierPage->checkForMenuItems($parameters);
    }

    /**
     * @param array $parameters
     * @return \Bundle\CoJemyCore\CoreBundle\Entity\FoodSupplier
     */
    private function createFoodSupplier(array $parameters = []) : FoodSupplier
    {
        $foodSupplier = (new FoodSupplierBuilder())
            ->withParameters($parameters)
            ->build();

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $em->persist($foodSupplier);
        $em->flush();
        
        return $foodSupplier;
    }

    /**
     * @param mixed $id
     * @param TableNode $menuItemsTable
     */
    private function createMenuItemsForFoodSupplierWithId($id, TableNode $menuItemsTable)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $foodSupplierRepository = $em->getRepository('CoreBundle:FoodSupplier');

        $foodSupplierBuilder = new FoodSupplierBuilder();

        foreach ($menuItemsTable->getHash() as $menuItemHash) {
            $foodSupplierBuilder->withMenuItemWithParameters($menuItemHash);
        }

        $foodSupplier = $foodSupplierRepository->find($id);

        $foodSupplierBuilder->buildMenuItemsForFoodSupplier($foodSupplier);

        foreach ($foodSupplier->getMenuItems() as $menuItem) {
            $em->persist($menuItem);
        }

        $em->merge($foodSupplier);
        $em->flush();
    }
    
    private function createFoodSupplierWithMenuItems(array $parameters = [])
    {
        $foodSupplier = $this->createFoodSupplier();

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        foreach ($foodSupplier->getMenuItems() as $menuItem) {
            $em->persist($menuItem);
        }

        $em->flush();
    }
}

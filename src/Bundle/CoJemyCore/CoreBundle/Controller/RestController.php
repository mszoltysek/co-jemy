<?php

namespace Bundle\CoJemyCore\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RestController extends Controller
{
    public function getSuppliersAction()
    {
        $suppliers = $this->getDoctrine()->getRepository('CoreBundle:FoodSupplier')->findAll();

        if (count($suppliers) === 0) {
            throw new NotFoundHttpException('Suppliers not found.');
        }

        return $suppliers;
    }

    public function getSupplierAction($id)
    {
        $supplier = $this->getDoctrine()->getRepository('CoreBundle:FoodSupplier')->find($id);

        if (is_null($supplier)) {
            throw new NotFoundHttpException('Supplier not found.');
        }

        return $supplier;
    }
}

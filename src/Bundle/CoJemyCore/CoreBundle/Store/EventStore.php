<?php

namespace Bundle\CoJemyCore\CoreBundle\Store;

use Doctrine\ORM\EntityRepository;

use Domain\CoJemy\Aggregate\AggregateId;
use Domain\CoJemy\EventStorePersister;
use Domain\CoJemy\EventStoreRepository;
use Domain\CoJemy\Order;
use Domain\CoJemy\Event as DomainEvent;

use Bundle\CoJemyCore\CoreBundle\Mapper\Event as EventMapper;

class EventStore extends EntityRepository implements EventStorePersister, EventStoreRepository
{
    /**
     * @param DomainEvent[] $events
     */
    public function persist(array $events)
    {
        $mapper = new EventMapper();

        foreach ($events as $event) {
            $this->getEntityManager()->persist($mapper->mapDomainToEntity($event));
        }

        $this->getEntityManager()->flush();
    }

    /**
     * @param AggregateId $id
     *
     * @return Order
     */
    public function findOrderById(AggregateId $id) : Order
    {
        $mapper = new EventMapper();

        $events = $this
            ->createQueryBuilder('e')
            ->where('e.aggregateId = :aggregateId')
            ->setParameter('aggregateId', $id)
            ->getQuery()
            ->getResult();

        $order = Order::recreate((string)$id, $mapper->mapEntitiesToDomain($events));

        return $order;
    }
}

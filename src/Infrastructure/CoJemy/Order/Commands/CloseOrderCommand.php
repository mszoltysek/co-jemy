<?php

namespace Infrastructure\CoJemy\Order\Commands;

use Domain\CoJemy\Aggregate\AggregateId;

class CloseOrderCommand
{
    /**
     * @var AggregateId
     */
    private $aggregateId;

    /**
     * CloseOrderCommand constructor.
     * @param AggregateId $aggregateId
     */
    public function __construct(AggregateId $aggregateId)
    {
        $this->aggregateId = $aggregateId;
    }

    /**
     * @return AggregateId
     */
    public function getAggregateId() : AggregateId
    {
        return $this->aggregateId;
    }
}
